#!/bin/bash

cd /etc/letsencrypt/live/

for site in $(ls); do
	certMail="$(cat ${site}/letsencrypt_email 2> /dev/null)"
	domainsList="$(grep -E '^[[:space:]]*server_name' /etc/nginx/sites-enabled/${site} | sort -u | cut -d' ' -f2- | tr '\n' ' ' | xargs | sed 's/ / -d /g' | tr -d ';' )"

	# Cleanup old sites
	[ -z "${domainsList}" ] &&
	{
		rm -rf ${site}
		rm /etc/letsencrypt/renewal/${site}*
		continue
	}

	[ ! -z "${certMail}" ] &&
	{
		mv "${site}" "old_${site}-old"
		certbot-auto certonly -n -m "${certMail}" -a webroot --webroot-path=/var/www/ssl-proof/ -d ${domainsList}
		CERTBO_OK=$?

		[ "$CERTBO_OK" == "0" ] &&
		{
			# Certbot successed let's reload nginx conf and delete old temp stuff

			[ -d "${site}" ] ||
			{
				# Certbot placed certs in wrong directory usually of the form ${site}-number
				mv "$(ls -d ${site}*)" "${site}"
			}

			rm -rf "old_${site}-old"
			nginx -s reload
		}

		[ "$CERTBO_OK" == "0" ] ||
		{
			# Certbot failed restore old temp stuff
			rm -rf "${site}"
			mv "old_${site}-old" "${site}"
		}
	}
done
