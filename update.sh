#!/bin/bash

cd "$( dirname "${BASH_SOURCE[0]}" )"

echo -n "" > lastlog

clExit()
{
  [ "x${1}" == "x0" ] ||
  {
    # Print script name, host name and error code
    echo -n "${BASH_SOURCE[0]} FAILED on $HOSTNAME with ${1}"
    
    # If exists, print error message
    [ "x${2}" == "x" ] && echo "" || echo ": ${2}"
    
    # Print log
    cat lastlog
    
  # Everything to stderr
  } >&2
  
  # Exit with errorcode $1
  exit ${1}
}

# Find all zone files
prunedFind()
{
  find . \( -path "./.git" -prune -or ! -path "./update.sh" -and ! -path "./lastlog" \) -type f -print
}

# Query DNS, exit nicely on error
myHost()
{
  # egrep -v returns 1 when regexp is found, AKA, dig returns no result or error (which begins with ';')
  dig "${1}" +short | egrep -v '^$|^;' || clExit 1 "Failed DNS lookup for '${1}'"
}

md5check()
{
	prunedFind | xargs md5sum | md5sum
}

# MD5 result to string into a variable (no tmpfile)
MD5SUMS="$(md5check)"

# Update from GIT
git fetch --all &>> lastlog
git reset --hard origin/master &>> lastlog

# If any change detected, check and apply new config
[ "${MD5SUMS}" == "$(md5check)" ] ||
{
	for site in $(ls sites-enabled/); do
		certMail="$(awk '/Letsencrypt email/ {print $NF;exit;}' sites-enabled/${site})"
		keyDir=/etc/letsencrypt/live/$site
		[ ! -z "${certMail}" ] && [ ! -d "${keyDir}" ] &&
		{
			mkdir -p "${keyDir}"
			echo "${certMail}" > "${keyDir}/letsencrypt_email"
			openssl req -x509 -nodes -days 1 -batch -newkey rsa:512 -keyout ${keyDir}/privkey.pem -out ${keyDir}/fullchain.pem
		}
	done

	mount --bind $(pwd)/sites-enabled/ /etc/nginx/sites-enabled/
	nginx -t
	EXITVALUE=$?
	umount /etc/nginx/sites-enabled/ || clExit 1 "Failed to umount config test dir"
	sleep 1s
	[ $EXITVALUE == 0 ] || clExit 1 "nginx config test failed"
	rsync --archive --delete sites-enabled/ /etc/nginx/sites-enabled/
	nginx -s reload
	((sleep 60s && ./letsencrypt_autocert.sh)&)
}

clExit 0
