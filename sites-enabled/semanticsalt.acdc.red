server {
	listen 80;
	listen [::]:80;

	listen 443 ssl;
	listen [::]:443 ssl;

	ssl_certificate /etc/letsencrypt/live/semanticsalt.acdc.red/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/semanticsalt.acdc.red/privkey.pem;

	server_name semanticsalt.acdc.red;

	# Letsencrypt email: efkin@cooperativa.cat
	location /.well-known { root /var/www/ssl-proof/; }
	
	location / {
		#access_log off;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_pass http://10.1.152.27;
	}
}

