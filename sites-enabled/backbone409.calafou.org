server {
	listen 80;
	listen [::]:80;

	server_name backbone409.calafou.org;

	location / {
		#access_log off;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		# Using a variable is just to avoid nginx crash at startup if it can't
		# resolve the target hostname
		# @see https://stackoverflow.com/q/32845674#comment61836356_32846603
		resolver 10.1.152.10 valid=300s;
		set $upstream_host micheleangiolillo.loc.aureasocial.org;
		proxy_pass http://$upstream_host$request_uri;
	}
}

