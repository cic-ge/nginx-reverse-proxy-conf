server {
	listen 80;
	listen [::]:80;

	server_name libreant.acdc.red;

	location / {
		#access_log off;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_pass http://10.1.152.27;
	}
}

